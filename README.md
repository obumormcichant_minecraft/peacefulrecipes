# Peaceful Recipes

This Minecraft data pack adds recipes for items normally obtained from
monsters.  It’s intended for players who prefer to play (mostly) in
peaceful mode.

The current build should be compatible with Minecraft Java edition
1.20.2 – 1.21.4 (data pack formats 18–61).

## Recipes

* #### Blaze Rods
  - 3 gunpowder
  - 1 gold ingot
  - 1 nether quartz
  - 1 bucket of lava

  Makes 4.  Gunpowder of course is normally farmed from creepers, but
  it’s possible to set up a relatively safe creeper or general monster
  farm.

* #### Breeze Rods (Minecraft 1.21+ only)
  - 3 gunpowder
  - 1 glass bottle
  - 1 nether quartz
  - 1 bucket of powdered snow

  Makes 4.

* #### Frog Lights
  - 1 slimeball
  - 1 magma block _or_ shroomlight
  - 1 lime, pink, or yellow dye

  Makes 1 verdant, pearlescent, or ochre froglight corresponding to
  the dye used.  See the slimeball recipe below.

* #### Heavy Core (Minecraft 1.21+ only)

  Cook an iron block in a blast furnace for 8 minutes 20 seconds,
  which is how long one lava bucket will burn.  (Cannot be cooked in a
  normal furnace.)

* #### Nether Star

  Cook glowstone in a blast furnace for 16 minutes 40 seconds, which
  is how long two lava buckets will burn.  (Cannot be cooked in a
  normal furnace.)

* #### Redstone Dust

  Cook a spider eye in a (normal) furnace.  Each eye takes 2½ seconds
  to cook, so you can smelt 4× as many eyes as most other items for a
  given amount of fuel.  Spider eyes of course must be farmed from
  spiders, but it’s possible to set up a relatively safe spider farm
  around spawners in an abandoned mineshaft.

* #### Resin Clump _(since Minecraft 1.21.4)_

  Cook a pale oak log in a smoker.  Resin normally is generated by
  attacking a “creaking”, though it can also drop by mining a creaking
  heart; this makes it (slightly) easier to obtain than finding
  creaking hearts.

* #### Shulker Shell
  - 4 turtle helmets arranged in a diamond shape

  Makes 1

* #### Slimeball
  - 3 spider eyes
  - green dye

  Makes 1

* #### Wither Rose
  - 1 dead coral block
  - 1 rose bush
  - 1 sculk vein

  Makes 3

## Usage

### New worlds

From the Create New World screen, go to the “More” tab, click on “Data
Packs”, and “Open Pack Folder”.  Copy
`PeacefulRecipes-`_version_`.zip` into this folder, then it will
appear in the list of “Available” data packs.  Click the rightward
arrow on the data pack icon to select it for your world.

### Existing worlds

From the “Select World” screen, click on your world (don’t click its
play button) then click the “Edit” button.  Click the “Open World
Folder” and then open the `datapacks` folder within it.  Copy
`PeacefulRecipes-`_version_`.zip` into this folder.  Return to
Minecraft’s Edit World screen and click “Cancel”.  Then start your
world; it will automatically load the new data pack.
